var express = require('express');
var app = express();

app.get('/', function(req, res){
	res.writeHead(200, {'Content-Type': 'text/plain'});
	res.end('Hello World!');
	console.log("Someone connected!");
});

app.post('/json', function(req, res) {

    console.dir(req.param);

    if (req.method == 'POST') {
        console.log("POST");
        var body = '';
        req.on('data', function (data) {
            body += data;
            console.log("Partial body: " + body);
        });
        req.on('end', function () {
            console.log("Body: " + body);
		});
		
		res.setHeader('Content-Type', 'application/json');
		var data = {'name': 'kentaurus', 'title': 'programmer'};
		res.send(JSON.stringify(data));
		console.log("json sent");
	
	}
});

app.listen(3000);